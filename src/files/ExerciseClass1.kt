package files

class ExerciseClass1 {

    companion object {
        fun m1(doubles: Array<Double>): Double {
            var count: Int = 0;
            var sum: Double = 0.0;

            for ((index, d) in doubles.withIndex()) {
                if(index % 2 == 0){
                    count++;
                    sum += d;
                }
            }

            return sum / count;
        }
    }
}