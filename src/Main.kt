import files.ExerciseClass1
import files.ExerciseClass2

fun main() {


    //დავალება 1

    //    დაწერეთ ფუნქცია, რომელიც პარამეტრად მიიღებს მთელი რიცხვების მასივს და დააბრუნებს
    //    მთელ მნიშვნელობას.ფუნქციამ უნდა გამოთვალოს მასივში ლუწ ინდექსზე მდგომი რიცხვების
    //    საშუალო არითმეტიკული.
    //    მოიყვანეთ ფუნქციის გამოყენების მაგალითი.
    //    (2.5ქ)
    val res1: Double = ExerciseClass1.m1( arrayOf<Double>(2.2, 3.5, 10.5, 23.1, 100.8, 120.9));
    println(res1);


    //დავალება 2

    //    დაწერეთ ფუნქცია, რომელსაც გადაეცემა String ტიპის პარამეტრი და დააბრუნებს Boolean
    //    მნიშვნელობას.ფუნქციამ უნდა დააბრუნოს true, თუ გადმოცემული String მნიშვნელობა
    //    პალინდრომია, წინააღმდეგ შემთხვევაში false.
    //    (პალინდრომი— სიტყვა, ფრაზა ან ლექსი, რომელიც წაღმა და უკუღმა ერთნაირად იკითხება)
    //    (2.5ქ)
    val res2: Boolean = ExerciseClass2.m1("aka");
    val res3: Boolean = ExerciseClass2.m1("nika tsitsilashvili");
    println(res2);
    println(res3);

}
